//
//  ViewController.h
//  AppleMaps
//
//  Created by click labs 115 on 10/13/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
@interface ViewController : UIViewController
@property (strong, nonatomic) IBOutlet MKMapView *appleMap;


@end

